# Project MotionPictures
## Content
Motion Pictures is a platform that lets you keep track of your movies and series. It is possible, among a large selection of movies and series, to save all the ones you have already seen or the ones you still want to see on your own list. In addition, each user has the possibility to rate a movie or series from 1 (Appalling) to 10 (Masterpiece) and write a review about it. Furthermore, after successfully creating and logging in your account, you have the possibility to change your username and profile picture. Every user profile provides a list of the user’s entries. It is up to them, whether the set this list public or private.
## Employees
+ **Zeller Gabriel**
+ Hafner Marlena
+ Kern Alexander
+ Pronhagl Julian
