﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Model.Entities
{
    [Table("seasons")]
    public partial class Season
    {
        [Key]
        [Column("tconst")]
        [StringLength(10)]
        public string Tconst { get; set; } = null!;
        [Key]
        [Column("SEASON_NR")]
        public int SeasonNr { get; set; }
        [Column("NR_OF_EPISODES")]
        public int NrOfEpisodes { get; set; }

        [ForeignKey(nameof(Tconst))]
        [InverseProperty(nameof(Show.Seasons))]
        public virtual Show TconstNavigation { get; set; } = null!;
    }
}
