﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Model.Entities
{
    [Table("users")]
    [Index(nameof(Email), Name = "EMAIL_UNIQUE", IsUnique = true)]
    [Index(nameof(Username), Name = "USERNAME_UNIQUE", IsUnique = true)]
    public partial class User
    {
        public User()
        {
            Reviews = new HashSet<Review>();
            UserHasLists = new HashSet<UserHasList>();
        }

        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("USERNAME")]
        [StringLength(45)]
        public string Username { get; set; } = null!;
        [Column("EMAIL")]
        public string Email { get; set; } = null!;
        [Column("PASSWORD")]
        [StringLength(45)]
        public string Password { get; set; } = null!;
        [Column("DATE_OF_BIRTH", TypeName = "datetime")]
        public DateTime? DateOfBirth { get; set; }
        [Column("CREATED_AT", TypeName = "datetime")]
        public DateTime CreatedAt { get; set; }
        
        [Column("ABOUT_ME")]
        [StringLength(255)]
        public string? AboutMe { get; set; }

        [InverseProperty(nameof(Review.Users))]
        public virtual ICollection<Review> Reviews { get; set; }
        [InverseProperty(nameof(UserHasList.Users))]
        public virtual ICollection<UserHasList> UserHasLists { get; set; }
    }
}
