﻿using System.ComponentModel.DataAnnotations;

namespace Model.Entities; 

public class UserDTO {
    [Required(ErrorMessage = "Username is required")]
    [MinLength(4, ErrorMessage = "Username has to be at least 4 characters long.")]
    [MaxLength(20, ErrorMessage = "Username cannot be longer than 20 characters.")]
    public string Username { get; set; }
    
    [Required(ErrorMessage = "Email is required")]
    [EmailAddress]
    [MaxLength(40, ErrorMessage = "Email cannot be longer than 40 characters.")]
    public string Email { get; set; }
    
    [Required(ErrorMessage = "Password is required")]
    [MinLength(4, ErrorMessage = "Password needs at least 4 characters.")]
    [MaxLength(20, ErrorMessage = "Password cannot be longer than 20 characters.")]
    public string Password { get; set; }
}