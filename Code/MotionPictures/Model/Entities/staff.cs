﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Model.Entities
{
    public partial class staff
    {
        public staff()
        {
            TitlesHasStaffs = new HashSet<TitlesHasStaff>();
        }

        [Key]
        [Column("nconst")]
        [StringLength(10)]
        public string Nconst { get; set; } = null!;
        [Column("NAME")]
        [StringLength(105)]
        public string Name { get; set; } = null!;

        [InverseProperty(nameof(TitlesHasStaff.NconstNavigation))]
        public virtual ICollection<TitlesHasStaff> TitlesHasStaffs { get; set; }
    }
}
