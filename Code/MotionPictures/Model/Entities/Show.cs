﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Model.Entities
{
    [Table("shows")]
    public partial class Show
    {
        public Show()
        {
            Seasons = new HashSet<Season>();
        }

        [Key]
        [Column("tconst")]
        [StringLength(10)]
        public string Tconst { get; set; } = null!;
        [Column("START_YEAR")]
        public int StartYear { get; set; }
        [Column("END_YEAR")]
        public int? EndYear { get; set; }
        [Column("EPISODE_RUNTIME")]
        public int? EpisodeRuntime { get; set; }

        [ForeignKey(nameof(Tconst))]
        [InverseProperty(nameof(Title.Show))]
        public virtual Title TconstNavigation { get; set; } = null!;
        [InverseProperty(nameof(Season.TconstNavigation))]
        public virtual ICollection<Season> Seasons { get; set; }
    }
}
