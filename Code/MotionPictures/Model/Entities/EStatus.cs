﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Model.Entities
{
    [Table("e_status")]
    public partial class EStatus
    {
        public EStatus()
        {
            UserHasLists = new HashSet<UserHasList>();
        }

        [Key]
        [Column("VALUE")]
        [StringLength(9)]
        public string Value { get; set; } = null!;

        [InverseProperty(nameof(UserHasList.StatusNavigation))]
        public virtual ICollection<UserHasList> UserHasLists { get; set; }
    }
}
