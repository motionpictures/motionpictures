﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Model.Entities
{
    [Table("ratings")]
    public partial class Rating
    {
        [Key]
        [Column("tconst")]
        [StringLength(10)]
        public string Tconst { get; set; } = null!;
        [Column("AVG_SCORE")]
        public float AvgScore { get; set; }
        [Column("VOTES")]
        public int Votes { get; set; }

        [ForeignKey(nameof(Tconst))]
        [InverseProperty(nameof(Title.Rating))]
        public virtual Title TconstNavigation { get; set; } = null!;
    }
}
