﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Model.Entities
{
    [Table("titles_has_staff")]
    [Index(nameof(Occupation), Name = "fk_TITLES_has_STAFF_E_OCCUPATION1_idx")]
    [Index(nameof(Nconst), Name = "fk_TITLES_has_STAFF_STAFF1_idx")]
    [Index(nameof(Tconst), Name = "fk_TITLES_has_STAFF_TITLES1_idx")]
    public partial class TitlesHasStaff
    {
        [Key]
        [Column("tconst")]
        [StringLength(10)]
        public string Tconst { get; set; } = null!;
        [Key]
        [Column("nconst")]
        [StringLength(10)]
        public string Nconst { get; set; } = null!;
        [Column("ROLE_PLAYED", TypeName = "text")]
        public string? RolePlayed { get; set; }
        [Column("OCCUPATION")]
        [StringLength(19)]
        public string Occupation { get; set; } = null!;
        [Key]
        [Column("ID")]
        public int Id { get; set; }

        [ForeignKey(nameof(Nconst))]
        [InverseProperty(nameof(staff.TitlesHasStaffs))]
        public virtual staff NconstNavigation { get; set; } = null!;
        [ForeignKey(nameof(Occupation))]
        [InverseProperty(nameof(EOccupation.TitlesHasStaffs))]
        public virtual EOccupation OccupationNavigation { get; set; } = null!;
        [ForeignKey(nameof(Tconst))]
        [InverseProperty(nameof(Title.TitlesHasStaffs))]
        public virtual Title TconstNavigation { get; set; } = null!;
    }
}
