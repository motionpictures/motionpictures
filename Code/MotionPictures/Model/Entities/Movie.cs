﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Model.Entities
{
    [Table("movies")]
    public partial class Movie
    {
        [Key]
        [Column("tconst")]
        [StringLength(10)]
        public string Tconst { get; set; } = null!;
        [Column("AIRED_IN")]
        public int AiredIn { get; set; }
        [Column("RUNTIME")]
        public int Runtime { get; set; }

        [ForeignKey(nameof(Tconst))]
        [InverseProperty(nameof(Title.Movie))]
        public virtual Title TconstNavigation { get; set; } = null!;
    }
}
