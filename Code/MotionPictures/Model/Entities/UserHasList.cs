﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Model.Entities
{
    [Table("user_has_lists")]
    [Index(nameof(Status), Name = "fk_USERS_has_LIST_E_STATUS1_idx")]
    [Index(nameof(Tconst), Name = "fk_USERS_has_TITLES_TITLES1_idx")]
    [Index(nameof(UsersId), Name = "fk_USERS_has_TITLES_USERS1_idx")]
    public partial class UserHasList
    {
        [Key]
        [Column("USERS_ID")]
        public int UsersId { get; set; }
        [Key]
        [Column("tconst")]
        [StringLength(10)]
        public string Tconst { get; set; } = null!;
        [Column("STATUS")]
        [StringLength(9)]
        public string Status { get; set; } = null!;

        [ForeignKey(nameof(Status))]
        [InverseProperty(nameof(EStatus.UserHasLists))]
        public virtual EStatus StatusNavigation { get; set; } = null!;
        [ForeignKey(nameof(Tconst))]
        [InverseProperty(nameof(Title.UserHasLists))]
        public virtual Title TconstNavigation { get; set; } = null!;
        [ForeignKey(nameof(UsersId))]
        [InverseProperty(nameof(User.UserHasLists))]
        public virtual User Users { get; set; } = null!;
    }
}
