﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Model.Entities
{
    [Table("genres")]
    public partial class Genre
    {
        public Genre()
        {
            Tconsts = new HashSet<Title>();
        }

        [Key]
        [Column("NAME")]
        [StringLength(26)]
        public string Name { get; set; } = null!;

        [ForeignKey("Genre")]
        [InverseProperty(nameof(Title.Genres))]
        public virtual ICollection<Title> Tconsts { get; set; }
    }
}
