﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Model.Entities
{
    [Table("e_occupation")]
    public partial class EOccupation
    {
        public EOccupation()
        {
            TitlesHasStaffs = new HashSet<TitlesHasStaff>();
        }

        [Key]
        [Column("VALUE")]
        [StringLength(19)]
        public string Value { get; set; } = null!;

        [InverseProperty(nameof(TitlesHasStaff.OccupationNavigation))]
        public virtual ICollection<TitlesHasStaff> TitlesHasStaffs { get; set; }
    }
}
