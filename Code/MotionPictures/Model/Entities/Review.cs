﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Model.Entities
{
    [Table("reviews")]
    [Index(nameof(Tconst), Name = "fk_REVIEWS_TITLES1_idx")]
    [Index(nameof(UsersId), Name = "fk_REVIEWS_USERS1_idx")]
    public partial class Review
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("CONTENT")]
        [StringLength(255)]
        public string Content { get; set; } = null!;
        [Column("SCORE")]
        public int Score { get; set; }
        [Key]
        [Column("USERS_ID")]
        public int UsersId { get; set; }
        [Key]
        [Column("tconst")]
        [StringLength(10)]
        public string Tconst { get; set; } = null!;

        [ForeignKey(nameof(Tconst))]
        [InverseProperty(nameof(Title.Reviews))]
        public virtual Title TconstNavigation { get; set; } = null!;
        [ForeignKey(nameof(UsersId))]
        [InverseProperty(nameof(User.Reviews))]
        public virtual User Users { get; set; } = null!;
    }
}
