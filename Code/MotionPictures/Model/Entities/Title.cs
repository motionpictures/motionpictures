﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Model.Entities
{
    [Table("titles")]
    public partial class Title
    {
        public Title()
        {
            Reviews = new HashSet<Review>();
            TitlesHasStaffs = new HashSet<TitlesHasStaff>();
            UserHasLists = new HashSet<UserHasList>();
            Genres = new HashSet<Genre>();
        }

        [Key]
        [Column("tconst")]
        [StringLength(10)]
        public string Tconst { get; set; } = null!;
        [Column("NAME", TypeName = "text")]
        public string Name { get; set; } = null!;
        [Column("IS_ADULT")]
        public sbyte IsAdult { get; set; }
        [Column("DESCRIPTION", TypeName = "text")]
        public string? Description { get; set; }
        [Column("IMAGE", TypeName = "text")]
        public string? Image { get; set; }

        [InverseProperty("TconstNavigation")]
        public virtual Movie Movie { get; set; } = null!;
        [InverseProperty("TconstNavigation")]
        public virtual Rating Rating { get; set; } = null!;
        [InverseProperty("TconstNavigation")]
        public virtual Show Show { get; set; } = null!;
        [InverseProperty(nameof(Review.TconstNavigation))]
        public virtual ICollection<Review> Reviews { get; set; }
        [InverseProperty(nameof(TitlesHasStaff.TconstNavigation))]
        public virtual ICollection<TitlesHasStaff> TitlesHasStaffs { get; set; }
        [InverseProperty(nameof(UserHasList.TconstNavigation))]
        public virtual ICollection<UserHasList> UserHasLists { get; set; }

        [ForeignKey("Tconst")]
        [InverseProperty(nameof(Genre.Tconsts))]
        public virtual ICollection<Genre> Genres { get; set; }
    }
}
