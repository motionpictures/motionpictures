﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Model.Entities;

namespace Model.Configurations
{
    public partial class MoPiDbContext : DbContext
    {
        public MoPiDbContext()
        {
        }

        public MoPiDbContext(DbContextOptions<MoPiDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<EOccupation> EOccupations { get; set; } = null!;
        public virtual DbSet<EStatus> EStatuses { get; set; } = null!;
        public virtual DbSet<Genre> Genres { get; set; } = null!;
        public virtual DbSet<Movie> Movies { get; set; } = null!;
        public virtual DbSet<Rating> Ratings { get; set; } = null!;
        public virtual DbSet<Review> Reviews { get; set; } = null!;
        public virtual DbSet<Season> Seasons { get; set; } = null!;
        public virtual DbSet<Show> Shows { get; set; } = null!;
        public virtual DbSet<Title> Titles { get; set; } = null!;
        public virtual DbSet<TitlesHasStaff> TitlesHasStaffs { get; set; } = null!;
        public virtual DbSet<User> Users { get; set; } = null!;
        public virtual DbSet<UserHasList> UserHasLists { get; set; } = null!;
        public virtual DbSet<staff> staff { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql("server=localhost;user=school;password=school;database=motionpicturesdb", Microsoft.EntityFrameworkCore.ServerVersion.Parse("8.0.28-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");

            modelBuilder.Entity<EOccupation>(entity =>
            {
                entity.HasKey(e => e.Value)
                    .HasName("PRIMARY");
            });

            modelBuilder.Entity<EStatus>(entity =>
            {
                entity.HasKey(e => e.Value)
                    .HasName("PRIMARY");
            });

            modelBuilder.Entity<Genre>(entity =>
            {
                entity.HasKey(e => e.Name)
                    .HasName("PRIMARY");
            });

            modelBuilder.Entity<Movie>(entity =>
            {
                entity.HasKey(e => e.Tconst)
                    .HasName("PRIMARY");

                entity.HasOne(d => d.TconstNavigation)
                    .WithOne(p => p.Movie)
                    .HasForeignKey<Movie>(d => d.Tconst)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Movies_TITLES1");
            });

            modelBuilder.Entity<Rating>(entity =>
            {
                entity.HasKey(e => e.Tconst)
                    .HasName("PRIMARY");

                entity.HasOne(d => d.TconstNavigation)
                    .WithOne(p => p.Rating)
                    .HasForeignKey<Rating>(d => d.Tconst)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_RATINGS_TITLES1");
            });

            modelBuilder.Entity<Review>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.UsersId, e.Tconst })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0, 0 });

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.HasOne(d => d.TconstNavigation)
                    .WithMany(p => p.Reviews)
                    .HasForeignKey(d => d.Tconst)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_REVIEWS_TITLES1");

                entity.HasOne(d => d.Users)
                    .WithMany(p => p.Reviews)
                    .HasForeignKey(d => d.UsersId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_REVIEWS_USERS1");
            });

            modelBuilder.Entity<Season>(entity =>
            {
                entity.HasKey(e => new { e.Tconst, e.SeasonNr })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.HasOne(d => d.TconstNavigation)
                    .WithMany(p => p.Seasons)
                    .HasForeignKey(d => d.Tconst)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_SEASONS_Shows1");
            });

            modelBuilder.Entity<Show>(entity =>
            {
                entity.HasKey(e => e.Tconst)
                    .HasName("PRIMARY");

                entity.HasOne(d => d.TconstNavigation)
                    .WithOne(p => p.Show)
                    .HasForeignKey<Show>(d => d.Tconst)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Shows_TITLES1");
            });

            modelBuilder.Entity<Title>(entity =>
            {
                entity.HasKey(e => e.Tconst)
                    .HasName("PRIMARY");

                entity.HasMany(d => d.Genres)
                    .WithMany(p => p.Tconsts)
                    .UsingEntity<Dictionary<string, object>>(
                        "TitleHasGenre",
                        l => l.HasOne<Genre>().WithMany().HasForeignKey("Genre").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("fk_TITLES_has_GENRES_GENRES1"),
                        r => r.HasOne<Title>().WithMany().HasForeignKey("Tconst").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("fk_TITLES_has_GENRES_TITLES"),
                        j =>
                        {
                            j.HasKey("Tconst", "Genre").HasName("PRIMARY").HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                            j.ToTable("title_has_genres");

                            j.HasIndex(new[] { "Genre" }, "fk_TITLES_has_GENRES_GENRES1_idx");

                            j.HasIndex(new[] { "Tconst" }, "fk_TITLES_has_GENRES_TITLES_idx");

                            j.IndexerProperty<string>("Tconst").HasMaxLength(10).HasColumnName("tconst");

                            j.IndexerProperty<string>("Genre").HasMaxLength(26).HasColumnName("GENRE");
                        });
            });

            modelBuilder.Entity<TitlesHasStaff>(entity =>
            {
                entity.HasKey(e => new { e.Tconst, e.Nconst, e.Id })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0, 0 });

                entity.HasOne(d => d.NconstNavigation)
                    .WithMany(p => p.TitlesHasStaffs)
                    .HasForeignKey(d => d.Nconst)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_TITLES_has_STAFF_STAFF1");

                entity.HasOne(d => d.OccupationNavigation)
                    .WithMany(p => p.TitlesHasStaffs)
                    .HasForeignKey(d => d.Occupation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_TITLES_has_STAFF_E_OCCUPATION1");

                entity.HasOne(d => d.TconstNavigation)
                    .WithMany(p => p.TitlesHasStaffs)
                    .HasForeignKey(d => d.Tconst)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_TITLES_has_STAFF_TITLES1");
            });

            modelBuilder.Entity<UserHasList>(entity =>
            {
                entity.HasKey(e => new { e.UsersId, e.Tconst })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.HasOne(d => d.StatusNavigation)
                    .WithMany(p => p.UserHasLists)
                    .HasForeignKey(d => d.Status)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_USERS_has_LIST_E_STATUS1");

                entity.HasOne(d => d.TconstNavigation)
                    .WithMany(p => p.UserHasLists)
                    .HasForeignKey(d => d.Tconst)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_USERS_has_TITLES_TITLES1");

                entity.HasOne(d => d.Users)
                    .WithMany(p => p.UserHasLists)
                    .HasForeignKey(d => d.UsersId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_USERS_has_TITLES_USERS1");
            });

            modelBuilder.Entity<staff>(entity =>
            {
                entity.HasKey(e => e.Nconst)
                    .HasName("PRIMARY");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
