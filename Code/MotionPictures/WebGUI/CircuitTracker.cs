﻿using Domain.Repositories.Interfaces;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Server.Circuits;
using Microsoft.AspNetCore.Components.Server.ProtectedBrowserStorage;

namespace WebGUI; 

public class CircuitTracker : CircuitHandler {
    private readonly ProtectedLocalStorage _localStorage;
    private readonly NavigationManager _nav;
    private readonly IUserHandler _userHandler;
    private readonly IUserRepository _userRepository;

    public CircuitTracker(IUserHandler userHandler, ProtectedLocalStorage localStorage,
        IUserRepository userRepository, NavigationManager nav) {
        _localStorage = localStorage;
        _userRepository = userRepository;
        _nav = nav;
        _userHandler = userHandler;
    }

    public override async Task OnConnectionUpAsync(Circuit circuit, CancellationToken cancellationToken) {
        await GetStorage(cancellationToken);
    }

    private async Task GetStorage(CancellationToken ct) {

        var id = (await _localStorage.GetAsync<string>("Id")).Value;
        if (id is null) {
            //_nav.NavigateTo("Login");
            return;
        }

        var usr = await _userRepository.ReadAsync(int.Parse(id), ct);

        _userHandler.User = usr;

    }
}