using Blazorise;
using Blazorise.Bootstrap;
using Domain.Repositories.Implementations;
using Domain.Repositories.Implementations.EntityRepositories;
using Domain.Repositories.Interfaces;
using Microsoft.AspNetCore.Components.Server.Circuits;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging.Abstractions;
using Model.Configurations;
using Model.Entities;
using WebGUI;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContextFactory<MoPiDbContext>(
    options => options.UseMySql(
        builder.Configuration.GetConnectionString("DefaultConnection"),
        new MySqlServerVersion(new Version(8, 0, 29))
    ).UseLoggerFactory(new NullLoggerFactory())
    //.UseLoggerFactory(new NullLoggerFactory()) is to remove mysql query logging
);

builder.Services.AddScoped<CircuitHandler, CircuitTracker>();
builder.Services.AddScoped<IUserHandler, UserHandler>();
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddScoped<IRepository<Review>, ReviewRepository>();


builder.Services.AddScoped<ITitleRepository, TitleRepository>();
builder.Services.AddScoped<IListRepository, ListRepository>();

builder.Services
    .AddBlazorise(options => { options.Immediate = true; })
    .AddBootstrapProviders();

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();
builder.Services.AddHttpClient();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment()) {
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseRouting();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");

app.Run();