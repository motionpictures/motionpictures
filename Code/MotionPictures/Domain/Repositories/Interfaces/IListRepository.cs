using Model.Entities;

namespace Domain.Repositories.Interfaces;

public interface IListRepository : IRepository<UserHasList>
{
    Task<List<UserHasList>> ReadUserListAsync(int userId, string type,string filter);
    Task<List<UserHasList>> SearchUserListAsync(int userId, string type,string titleName,string filter);
    Task<UserHasList> ReadUserList(int userId,string tconst);
}