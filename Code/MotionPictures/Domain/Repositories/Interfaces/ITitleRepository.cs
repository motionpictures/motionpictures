using Model.Entities;

namespace Domain.Repositories.Interfaces;

public interface ITitleRepository : IRepository<Title>
{
    Task<List<Title>> ReadPageAsync(int start, int count, string filter);
    Task<Title?> ReadTitleAsync(string tconst);
    Task<List<Title>> SearchTitleAsync(string titleName,int start, int count, string filter);
}