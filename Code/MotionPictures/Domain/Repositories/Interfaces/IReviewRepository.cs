﻿using Model.Entities;

namespace Domain.Repositories.Interfaces; 

public interface IReviewRepository : IRepository<Review> {
    
}