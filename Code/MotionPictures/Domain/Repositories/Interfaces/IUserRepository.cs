﻿using System.Linq.Expressions;
using Model.Entities;

namespace Domain.Repositories.Interfaces; 

public interface IUserRepository : IRepository<User> {
    Task<User> ReadAuthGraphAsync(int id, CancellationToken ct);

    Task<User> ReadAuthGraphAsync(string username, CancellationToken ct);
    
    Task<List<User>> ReadGraphAsync(Expression<Func<User, bool>> filter);
    
    Task<User?> ReadGraphAsync(int id);
    Task<List<User>> ReadPageAsync(int start, int count, string filter);
    Task<List<User>> SearchUserAsync(string userName,int start, int count, string filter);
}