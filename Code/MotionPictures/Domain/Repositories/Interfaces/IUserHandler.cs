﻿using Model.Entities;

namespace Domain.Repositories.Interfaces; 

public interface IUserHandler {
    User User { get; set; }

    event Action? DataChange;
    void NotifyDataChange();
    string Encrypt(string plainText);
    string Decrypt(string cipherText);
}