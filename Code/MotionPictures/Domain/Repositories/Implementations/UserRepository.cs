﻿using System.Linq.Expressions;
using Domain.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Model.Configurations;
using Model.Entities;

namespace Domain.Repositories.Implementations; 

public class UserRepository : ARepository<User>, IUserRepository {
    public UserRepository(MoPiDbContext context) : base(context) { }

    public async Task<User> ReadAuthGraphAsync(int id, CancellationToken ct) {
        return await _set
            .SingleOrDefaultAsync(u => u.Id == id, ct) ?? null!;
    }
    public async Task<User> ReadAuthGraphAsync(string username, CancellationToken ct) {
        return await _set
            .SingleOrDefaultAsync(u => u.Username == username, ct) ?? null!;
    }
    public async Task<List<User>> ReadGraphAsync(Expression<Func<User, bool>> filter)
        => await _set.Where(filter).ToListAsync();
    
    public async Task<User?> ReadGraphAsync(int id)=> await _set.SingleOrDefaultAsync(u => u.Id == id);
    public async Task<List<User>> ReadPageAsync(int start, int count, string filter) {
        switch (filter) {
            case "reviews":
                return await _context.Users.OrderByDescending(u => u.Reviews.Count).Include(user => user.Reviews).Skip(start).Take(count).ToListAsync();
            case "name":
                return await _context.Users.OrderByDescending(u => u.Username).Include(user => user.Reviews).Skip(start).Take(count).ToListAsync();
            case "joined":
                return await _context.Users.OrderByDescending(u => u.CreatedAt).Include(user => user.Reviews).Skip(start).Take(count).ToListAsync();
        }
        return await _context.Users.Include(user => user.Reviews).Skip(start).Take(count).ToListAsync();
    }
    public async Task<List<User>> SearchUserAsync(string userName, int start, int count, string filter) {
        switch (filter) {
            case "reviews":
                return await _context.Users.OrderByDescending(u => u.Reviews.Count).Where(s => s.Username.ToLower().Contains(userName.ToLower())).Include(user => user.Reviews).Skip(start).Take(count).ToListAsync();
            case "name":
                return await _context.Users.OrderByDescending(u => u.Username).Where(s => s.Username.ToLower().Contains(userName.ToLower())).Include(user => user.Reviews).Skip(start).Take(count).ToListAsync();
            case "joined":
                return await _context.Users.OrderByDescending(u => u.CreatedAt).Where(s => s.Username.ToLower().Contains(userName.ToLower())).Include(user => user.Reviews).Skip(start).Take(count).ToListAsync();
        }
        return await _context.Users.Where(s => s.Username.ToLower().Contains(userName.ToLower())).Skip(start).Take(count).Include(user => user.Reviews).ToListAsync();
    }
}