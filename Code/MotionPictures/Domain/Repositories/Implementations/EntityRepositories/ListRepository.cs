using System.Linq.Expressions;
using Domain.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Model.Configurations;
using Model.Entities;

namespace Domain.Repositories.Implementations.EntityRepositories;

public class ListRepository : ARepository<UserHasList>, IListRepository
{
    public ListRepository(MoPiDbContext context) : base(context)
    {
    }

    public async Task<List<UserHasList>> ReadUserListAsync(int userId, string type, string filter)
    {
        switch (filter) {
            case "popularity":
                return _context.UserHasLists.OrderByDescending(uhl => uhl.TconstNavigation.Rating.Votes).Where(thl => thl.UsersId.Equals(userId)&&thl.Status.Equals(type)).Include(list => list.TconstNavigation).ThenInclude(title => title.Rating).ToList();
            case "score":
                return _context.UserHasLists.OrderByDescending(uhl => uhl.TconstNavigation.Rating.AvgScore).Where(thl => thl.UsersId.Equals(userId)&&thl.Status.Equals(type)).Include(list => list.TconstNavigation).ThenInclude(title => title.Rating).ToList();
            case "release":
                return _context.UserHasLists.OrderByDescending(uhl => uhl.TconstNavigation.Movie.AiredIn).ThenByDescending(uhl => uhl.TconstNavigation.Show.StartYear).Where(thl => thl.UsersId.Equals(userId)&&thl.Status.Equals(type)).Include(list => list.TconstNavigation).ThenInclude(title => title.Rating).ToList();
        }
        return _context.UserHasLists.OrderByDescending(uhl => uhl.TconstNavigation.Rating.AvgScore).Where(thl => thl.UsersId.Equals(userId)&&thl.Status.Equals(type)).Include(list => list.TconstNavigation).ThenInclude(title => title.Rating).ToList();
    }

    public async Task<List<UserHasList>> SearchUserListAsync(int userId, string type, string titleName, string filter)
    {
        switch (filter) {
            case "popularity":
                return _context.UserHasLists.OrderByDescending(uhl => uhl.TconstNavigation.Rating.Votes).Where(thl => thl.TconstNavigation.Name.ToLower().Contains(titleName.ToLower())&&thl.UsersId.Equals(userId)&&thl.Status.Equals(type)).Include(list => list.TconstNavigation).ThenInclude(title => title.Rating).ToList();
            case "score":
                return _context.UserHasLists.OrderByDescending(uhl => uhl.TconstNavigation.Rating.AvgScore).Where(thl => thl.TconstNavigation.Name.ToLower().Contains(titleName.ToLower())&&thl.UsersId.Equals(userId)&&thl.Status.Equals(type)).Include(list => list.TconstNavigation).ThenInclude(title => title.Rating).ToList();
            case "release":
                return _context.UserHasLists.OrderByDescending(uhl => uhl.TconstNavigation.Movie.AiredIn).ThenByDescending(uhl => uhl.TconstNavigation.Show.StartYear).Where(thl => thl.TconstNavigation.Name.ToLower().Contains(titleName.ToLower())&&thl.UsersId.Equals(userId)&&thl.Status.Equals(type)).Include(list => list.TconstNavigation).ThenInclude(title => title.Rating).ToList();
        }
        return _context.UserHasLists.OrderByDescending(uhl => uhl.TconstNavigation.Rating.AvgScore).Where(thl => thl.TconstNavigation.Name.ToLower().Contains(titleName.ToLower())&&thl.UsersId.Equals(userId)&&thl.Status.Equals(type)).Include(list => list.TconstNavigation).ThenInclude(title => title.Rating).ToList();
    }

    public async Task<UserHasList> ReadUserList(int userId,string tconst)
    {
        return await _set.SingleOrDefaultAsync(le => le.UsersId == userId && le.Tconst == tconst);
    }
}