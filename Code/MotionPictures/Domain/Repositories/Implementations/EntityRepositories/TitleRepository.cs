using System.Linq.Expressions;
using Domain.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Model.Configurations;
using Model.Entities;

namespace Domain.Repositories.Implementations.EntityRepositories;

public class TitleRepository : ARepository<Title>, ITitleRepository
{
    public TitleRepository(MoPiDbContext context) : base(context)
    {
    }

    public async Task<List<Title>> ReadPageAsync(int start, int count, string filter)
    { 
        switch (filter) {
            case "popularity":
                return await _context.Titles.OrderByDescending(title => title.Rating.Votes).Include(title => title.Rating).Skip(start).Take(count).ToListAsync();
            case "score":
                return await _context.Titles.OrderByDescending(title => title.Rating.AvgScore).ThenByDescending(title => title.Rating.Votes).Include(title => title.Rating).Skip(start).Take(count).ToListAsync();
            case "release":
                return await _context.Titles.OrderByDescending(title => title.Movie.AiredIn).ThenByDescending(title => title.Show.StartYear).ThenBy(title => title.Name).Include(title => title.Rating).Skip(start).Take(count).ToListAsync();
        }
        return await _context.Titles.OrderByDescending(title => title.Rating.AvgScore).ThenByDescending(title => title.Rating.Votes).Include(title => title.Rating).Skip(start).Take(count).ToListAsync();
    }

    public async Task<Title?> ReadTitleAsync(string tconst)
    {
        return await _set.Include(title => title.TitlesHasStaffs).ThenInclude(ths => ths.NconstNavigation).Include(title => title.Reviews).ThenInclude(review => review.Users).SingleOrDefaultAsync(t => t.Tconst == tconst);
    }

    public async Task<List<Title>> SearchTitleAsync(string titleName, int start, int count, string filter)
    {
        switch (filter) {
            case "popularity":
                return await _context.Titles.OrderByDescending(title => title.Rating.Votes).Where(s => s.Name.ToLower().Contains(titleName.ToLower())).Skip(start).Take(count).Include(title => title.Rating).ToListAsync();
            case "score":
                return await _context.Titles.OrderByDescending(title => title.Rating.AvgScore).ThenByDescending(title => title.Rating.Votes).Where(s => s.Name.ToLower().Contains(titleName.ToLower())).Skip(start).Take(count).Include(title => title.Rating).ToListAsync();
            case "release":
                return await _context.Titles.OrderByDescending(title => title.Movie.AiredIn).ThenByDescending(title => title.Show.StartYear).Where(s => s.Name.ToLower().Contains(titleName.ToLower())).Skip(start).Take(count).Include(title => title.Rating).ToListAsync();
        }
        return await _context.Titles.OrderByDescending(title => title.Rating.AvgScore).Where(s => s.Name.ToLower().Contains(titleName.ToLower())).Skip(start).Take(count).Include(title => title.Rating).ToListAsync();
    }
}