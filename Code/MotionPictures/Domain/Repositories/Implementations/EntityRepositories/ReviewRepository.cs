﻿using Domain.Repositories.Interfaces;
using Model.Configurations;
using Model.Entities;

namespace Domain.Repositories.Implementations.EntityRepositories; 

public class ReviewRepository : ARepository<Review>, IReviewRepository {
    public ReviewRepository(MoPiDbContext context) : base(context) { }
}