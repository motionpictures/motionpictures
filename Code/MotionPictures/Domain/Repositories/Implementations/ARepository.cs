﻿using System.Linq.Expressions;
using Domain.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Model.Configurations;

namespace Domain.Repositories.Implementations; 

public abstract class ARepository<TEntity> : IRepository<TEntity> where TEntity : class {
    protected readonly MoPiDbContext _context;
    protected readonly DbSet<TEntity> _set;

    protected ARepository(MoPiDbContext context) {
        _context = context;
        this._set = _context.Set<TEntity>();
    }

    public async Task<List<TEntity>> ReadAsync(CancellationToken ct) {
        return await _set.ToListAsync(ct);
    }
    public async Task<List<TEntity>> ReadAsync(int start, int count) {
        return await _set.Skip(start).Take(count).ToListAsync();
    }

    public async Task<List<TEntity>> ReadAllAsync() => await _set.ToListAsync();

    public async Task<TEntity?> ReadAsync(string id) {
        return await _set.FindAsync(id);
    }
    public async Task<TEntity?> ReadAsync(int id, CancellationToken ct) {
        return await _set.FindAsync(new object?[] {id}, ct);
    }

    public async Task<List<TEntity>> ReadAsync(Expression<Func<TEntity, bool>> filter, CancellationToken ct) {
        return await _set.Where(filter).ToListAsync(ct);
    }

    public async Task<TEntity> CreateAsync(TEntity entity, CancellationToken ct) {
        _set.Add(entity);
        await _context.SaveChangesAsync(ct);
        return entity;
    }

    public async Task<List<TEntity>> CreateAsync(List<TEntity> entity, CancellationToken ct) {
        _set.AddRange(entity);
        await _context.SaveChangesAsync(ct);
        return entity;
    }

    public async Task UpdateAsync(TEntity entity, CancellationToken ct) {
        _context.ChangeTracker.Clear();
        _set.Update(entity);
        await _context.SaveChangesAsync(ct);
    }

    public async Task UpdateAsync(IEnumerable<TEntity> entity, CancellationToken ct) {
        _set.UpdateRange(entity);
        await _context.SaveChangesAsync(ct);
    }

    public async Task DeleteAsync(TEntity entity, CancellationToken ct) {
        _context.ChangeTracker.Clear();
        _set.Remove(entity);
        await _context.SaveChangesAsync(ct);
    }

    public async Task DeleteAsync(IEnumerable<TEntity> entity, CancellationToken ct) {
        _set.RemoveRange(entity);
        await _context.SaveChangesAsync(ct);
    }

    public async Task DeleteAsync(Expression<Func<TEntity, bool>> filter, CancellationToken ct) {
        _set.RemoveRange(_set.Where(filter));
        await _context.SaveChangesAsync(ct);
    }
}