﻿using System.Security.Cryptography;
using System.Text;
using Domain.Repositories.Interfaces;
using Model.Entities;

namespace Domain.Repositories.Implementations; 

public class UserHandler : IUserHandler {

    public User User { get; set; } = new();

    public event Action? DataChange;

    public void NotifyDataChange() => DataChange?.Invoke();

    public string Encrypt(string plainText) {
        var iv = new byte[16];
        byte[] array;

        using (var aes = Aes.Create()) {
            aes.Key = Encoding.UTF8.GetBytes("qwertzuiopüasdfghjklöäyxcvbnm");
            aes.IV = iv;

            var encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

            using (var memoryStream = new MemoryStream()) {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write)) {
                    using (var streamWriter = new StreamWriter(cryptoStream)) {
                        streamWriter.Write(plainText);
                    }

                    array = memoryStream.ToArray();
                }
            }
        }

        return Convert.ToBase64String(array);
    }
    public string Decrypt(string cipherText) {
        var iv = new byte[16];
        var buffer = Convert.FromBase64String(cipherText);

        using var aes = Aes.Create();
        aes.Key = Encoding.UTF8.GetBytes("qwertzuiopüasdfghjklöäyxcvbnm");
        aes.IV = iv;
        var decrypt = aes.CreateDecryptor(aes.Key, aes.IV);

        using (var memoryStream = new MemoryStream(buffer)) {
            using (var cryptoStream = new CryptoStream(memoryStream, decrypt, CryptoStreamMode.Read)) {
                using (var streamReader = new StreamReader(cryptoStream)) {
                    return streamReader.ReadToEnd();
                }
            }
        }
    }
}